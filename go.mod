module go.jolheiser.com/bk

go 1.16

require (
	github.com/urfave/cli/v2 v2.3.0
	go.etcd.io/bbolt v1.3.5
	go.jolheiser.com/beaver v1.1.1
)
