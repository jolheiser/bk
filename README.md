# bk

~~🍔👑~~

Backup your memory!

[Help](HELP.txt)

## Motivation

Whenever I go to deploy a new binary, I have a habit of `mv`ing the
file to the same name with a `.bk` extension, just in case the new one fails.

The problem is...I usually forget to remove it if the new file works cleanly.

`bk` allows me to `bk file`, which moves it to `file.bk` and updates a local databse
with the expiration time (set via `--for <duration>` or `--until <time>`)  
Next time `bk --clean` is invoked (via cron or systemd timer, perhaps?),
`bk` checks the local DB and attempts to remove expired backups.

## Systemd

To get a sample systemd service (`bk.service`)
```
bk --systemd-service
```

To get a sample systemd timer (`bk.timer`)
```
bk --systemd-timer
```

## License

[MIT](LICENSE)