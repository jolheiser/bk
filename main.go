//go:generate go run docs.go

package main

import (
	"os"

	"go.jolheiser.com/bk/cmd"

	"go.jolheiser.com/beaver"
)

func main() {
	if err := cmd.New().Run(os.Args); err != nil {
		beaver.Error(err)
	}
}
