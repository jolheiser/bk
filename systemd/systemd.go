package systemd

import (
	_ "embed"
	"os"
	"strings"
)

var (
	//go:embed service
	Service string
	//go:embed timer
	Timer string
)

func init() {
	bin, err := os.Executable()
	if err != nil {
		return
	}
	Service = os.Expand(Service, func(s string) string {
		if strings.EqualFold(s, "bin") {
			return bin
		}
		return ""
	})
}
