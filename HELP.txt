NAME:
   bk - Backup

USAGE:
   bk [global options] [arguments...]

GLOBAL OPTIONS:
   --database value  Path to the backup database (default: ${HOME}/.bk.db) [$BK_DATABASE]
   --clean           Clean up backed up files from DB (default: false)
   --undo            Undo file backups (default: false)
   --status          Check backup status (default: false)
   --for value       Back up file for x duration (default: 24h0m0s)
   --until value     Back up file until x
   --help, -h        show help (default: false)
