package cmd

import (
	"fmt"

	"go.jolheiser.com/bk/cmd/flags"
	"go.jolheiser.com/bk/systemd"

	"github.com/urfave/cli/v2"
)

func Systemd(_ *cli.Context) error {
	if flags.SystemdService {
		fmt.Println(systemd.Service)
	}
	if flags.SystemdTimer {
		fmt.Println(systemd.Timer)
	}
	return nil
}
