package cmd

import (
	"errors"
	"os"
	"path/filepath"
	"strings"
	"time"

	"go.jolheiser.com/bk/cmd/flags"
	"go.jolheiser.com/bk/database"

	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
	"go.jolheiser.com/beaver/color"
)

func Backup(ctx *cli.Context) error {
	if ctx.NArg() == 0 {
		return errors.New("bk needs at least one path to backup")
	}

	db, err := database.Load(flags.Database)
	if err != nil {
		return err
	}

	now := time.Now()
	expiration := now.Add(flags.For)
	if !flags.UntilTime.IsZero() {
		expiration = flags.UntilTime
	}

	success := make([]string, 0)
	fail := make([]string, 0)
	for _, arg := range ctx.Args().Slice() {
		if strings.HasSuffix(arg, ".bk") {
			fail = append(fail, arg)
			beaver.Errorf("%s already has .bk extension", arg)
			continue
		}

		fi, err := os.Stat(arg)
		if err != nil {
			fail = append(fail, arg)
			beaver.Errorf("could not backup %s: %v", arg, err)
			continue
		}
		if fi.IsDir() {
			fail = append(fail, arg)
			beaver.Errorf("cannot backup directory %s", arg)
			continue
		}

		apn, err := filepath.Abs(arg)
		if err != nil {
			fail = append(fail, arg)
			beaver.Errorf("could not backup %s: %v", arg, err)
			continue
		}

		bk := database.Backup{
			Path:       apn,
			Expiration: expiration,
		}

		if err := os.Rename(apn, bk.BKPath()); err != nil {
			fail = append(fail, arg)
			beaver.Errorf("could not backup %s to %s: %v", apn, bk.BKPath(), err)
			continue
		}

		if _, err := db.AddBackup(database.Backup{
			Path:       apn,
			Expiration: expiration,
		}); err != nil {
			fail = append(fail, arg)
			beaver.Errorf("could not backup %s: %v", arg, err)
			continue
		}
		success = append(success, arg)
	}

	if len(success) > 0 {
		beaver.Infof("The following files are backed up until a clean runs at %s:", color.FgBlue.Format(expiration.Format(timeLayout())))
		beaver.Info(color.FgYellow.Format(strings.Join(success, "\n")))
	}
	if len(fail) > 0 {
		beaver.Info("The following files failed to be backed up:")
		beaver.Info(color.FgYellow.Format(strings.Join(fail, "\n")))
	}
	return nil
}
