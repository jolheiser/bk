package cmd

import (
	"os"
	"time"

	"go.jolheiser.com/bk/cmd/flags"
	"go.jolheiser.com/bk/database"

	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
	"go.jolheiser.com/beaver/color"
)

func Clean(_ *cli.Context) error {
	db, err := database.Load(flags.Database)
	if err != nil {
		return err
	}

	backups, err := db.ListBackups()
	if err != nil {
		return err
	}

	now := time.Now()
	seqs := make([][]byte, 0)
	for _, backup := range backups {
		if now.After(backup.Expiration) {
			beaver.Infof("Removing %s", color.FgYellow.Format(backup.BKPath()))
			seqs = append(seqs, backup.Seq)
			if err := os.Remove(backup.BKPath()); err != nil {
				beaver.Errorf("could not remove %s: %v", backup.BKPath(), err)
				continue
			}
		}
	}

	return db.RemoveBackups(seqs...)
}
