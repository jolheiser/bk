package cmd

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	"go.jolheiser.com/bk/cmd/flags"

	"github.com/urfave/cli/v2"
)

func New() *cli.App {
	app := cli.NewApp()
	app.Name = "bk"
	app.Usage = "Backup"
	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:        "database",
			Usage:       "Path to the backup database",
			Value:       dbPath(),
			DefaultText: "${HOME}/.bk.db",
			EnvVars:     []string{"BK_DATABASE"},
			Destination: &flags.Database,
		},
		&cli.BoolFlag{
			Name:        "clean",
			Usage:       "Clean up backed up files from DB",
			Destination: &flags.Clean,
		},
		&cli.BoolFlag{
			Name:        "undo",
			Usage:       "Undo file backups",
			Destination: &flags.Undo,
		},
		&cli.BoolFlag{
			Name:        "status",
			Usage:       "Check backup status",
			Destination: &flags.Status,
		},
		&cli.DurationFlag{
			Name:        "for",
			Usage:       "Back up file for x duration",
			Value:       time.Hour * 24,
			Destination: &flags.For,
		},
		&cli.StringFlag{
			Name:        "until",
			Usage:       "Back up file until x",
			Destination: &flags.Until,
		},
		&cli.BoolFlag{
			Name:        "systemd-service",
			Usage:       "Output systemd service",
			Destination: &flags.SystemdService,
			Hidden:      true,
		},
		&cli.BoolFlag{
			Name:        "systemd-timer",
			Usage:       "Output systemd timer",
			Destination: &flags.SystemdTimer,
			Hidden:      true,
		},
	}
	app.Before = Before
	app.Action = Action
	app.HideHelpCommand = true
	return app
}

func Action(ctx *cli.Context) error {
	if flags.SystemdService || flags.SystemdTimer {
		return Systemd(ctx)
	}
	if flags.Undo {
		return Undo(ctx)
	}
	if flags.Clean {
		return Clean(ctx)
	}
	if flags.Status {
		return Status(ctx)
	}
	return Backup(ctx)
}

func Before(ctx *cli.Context) error {
	if err := exclusiveFlags(ctx, "systemd-service", "systemd-timer"); err != nil {
		return err
	}
	if err := exclusiveFlags(ctx, "clean", "undo", "status"); err != nil {
		return err
	}
	if err := exclusiveFlags(ctx, "for", "until"); err != nil {
		return err
	}
	if ctx.IsSet("until") {
		t, err := time.Parse(timeLayout(), flags.Until)
		if err != nil {
			return err
		}
		if t.Before(time.Now()) {
			return errors.New("--until cannot be in the past")
		}
		flags.UntilTime = t
	}
	return nil
}

func exclusiveFlags(ctx *cli.Context, flagNames ...string) error {
	var set bool
	for _, flagName := range flagNames {
		if ctx.IsSet(flagName) {
			if set {
				return fmt.Errorf("flags %s cannot be used together", strings.Join(flagNames, ", "))
			}
			set = true
		}
	}
	return nil
}

func dbPath() string {
	dir, err := os.UserHomeDir()
	if err != nil {
		bin, err := os.Executable()
		if err != nil {
			return "."
		}
		dir = filepath.Dir(bin)
	}
	return filepath.Join(dir, ".bk.db")
}

func timeLayout() string {
	layout := os.Getenv("BK_UNTIL_LAYOUT")
	if layout != "" {
		return layout
	}
	return "01/02/2006 15:04:05"
}
