package flags

import (
	"time"
)

var (
	Database string
	Clean    bool
	Undo     bool
	Status   bool

	For       time.Duration
	Until     string
	UntilTime time.Time

	SystemdService bool
	SystemdTimer   bool
)
