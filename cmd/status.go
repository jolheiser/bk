package cmd

import (
	"time"

	"go.jolheiser.com/bk/cmd/flags"
	"go.jolheiser.com/bk/database"

	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
	"go.jolheiser.com/beaver/color"
)

func Status(_ *cli.Context) error {
	db, err := database.Load(flags.Database)
	if err != nil {
		return err
	}

	backups, err := db.ListBackups()
	if err != nil {
		return err
	}

	now := time.Now()
	for _, backup := range backups {
		status := color.FgGreen
		if now.After(backup.Expiration) {
			status = color.FgRed
		}
		duration := backup.Expiration.Sub(now)
		beaver.Infof("%s %s", status.Format(backup.BKPath()), color.FgBlue.Formatf("(%s)", duration.Round(time.Second)))

	}

	return nil
}
