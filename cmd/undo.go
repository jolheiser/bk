package cmd

import (
	"errors"
	"os"
	"path/filepath"
	"strings"

	"go.jolheiser.com/bk/cmd/flags"
	"go.jolheiser.com/bk/database"

	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
	"go.jolheiser.com/beaver/color"
)

func Undo(ctx *cli.Context) error {
	if ctx.NArg() == 0 {
		return errors.New("bk needs at least one path to undo backup")
	}

	db, err := database.Load(flags.Database)
	if err != nil {
		return err
	}

	backups, err := db.ListBackups()
	if err != nil {
		return err
	}

	backupMap := make(map[string]database.Backup)
	for _, backup := range backups {
		backupMap[backup.Path] = backup
	}

	success := make([]string, 0)
	fail := make([]string, 0)
	seqs := make([][]byte, 0)
	for _, arg := range ctx.Args().Slice() {
		_, err := os.Stat(arg)
		if err != nil {
			fail = append(fail, arg)
			beaver.Errorf("could not undo backup %s: %v", arg, err)
			continue
		}

		uPath, err := filepath.Abs(arg)
		if err != nil {
			fail = append(fail, arg)
			beaver.Errorf("could not undo backup %s: %v", arg, err)
			continue
		}
		uPath = strings.TrimSuffix(uPath, filepath.Ext(uPath))

		b, ok := backupMap[uPath]
		if !ok {
			beaver.Warnf("file %s was not found in the DB, please rename it manually", uPath)
			continue
		}

		if err := os.Rename(arg, b.Path); err != nil {
			fail = append(fail, arg)
			beaver.Errorf("could not undo backup %s to %s: %v", arg, b.Path, err)
			continue
		}

		seqs = append(seqs, b.Seq)
		success = append(success, arg)
	}

	if len(success) > 0 {
		beaver.Info("The following files are un-backed up:")
		beaver.Info(color.FgYellow.Format(strings.Join(success, "\n")))
	}
	if len(fail) > 0 {
		beaver.Info("The following files failed to be un-backed up:")
		beaver.Info(color.FgYellow.Format(strings.Join(fail, "\n")))
	}
	return db.RemoveBackups(seqs...)
}
