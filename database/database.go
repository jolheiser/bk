package database

import (
	"encoding/json"
	"os"
	"strconv"
	"time"

	"go.etcd.io/bbolt"
)

var backupBucket = []byte("backups")

type Database struct {
	db *bbolt.DB
}

type Backup struct {
	Seq        []byte
	Path       string
	Expiration time.Time
}

func (b Backup) BKPath() string {
	return b.Path + ".bk"
}

func Load(dbPath string) (*Database, error) {
	db, err := bbolt.Open(dbPath, os.ModePerm, nil)
	if err != nil {
		return nil, err
	}
	if err := db.Update(func(tx *bbolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(backupBucket)
		return err
	}); err != nil {
		return nil, err
	}
	return &Database{
		db: db,
	}, nil
}

func (d *Database) AddBackup(b Backup) (uint64, error) {
	var seq uint64
	if err := d.db.Update(func(tx *bbolt.Tx) error {
		data, err := json.Marshal(b)
		if err != nil {
			return err
		}
		bucket := tx.Bucket(backupBucket)
		seq, err = bucket.NextSequence()
		if err != nil {
			return err
		}
		return bucket.Put([]byte(strconv.FormatUint(seq, 10)), data)
	}); err != nil {
		return 0, err
	}
	return seq, nil
}

func (d *Database) RemoveBackups(seqs ...[]byte) error {
	return d.db.Batch(func(tx *bbolt.Tx) error {
		bucket := tx.Bucket(backupBucket)
		for _, seq := range seqs {
			if err := bucket.Delete(seq); err != nil {
				return err
			}
		}
		return nil
	})
}

func (d *Database) ListBackups() ([]Backup, error) {
	backups := make([]Backup, 0)
	if err := d.db.View(func(tx *bbolt.Tx) error {
		return tx.Bucket(backupBucket).ForEach(func(seq, data []byte) error {
			var backup Backup
			if err := json.Unmarshal(data, &backup); err != nil {
				return err
			}
			backup.Seq = seq
			backups = append(backups, backup)
			return nil
		})
	}); err != nil {
		return nil, err
	}
	return backups, nil
}
