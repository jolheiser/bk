package database

import (
	"os"
	"path/filepath"
	"strconv"
	"testing"
	"time"
)

var db *Database

func TestMain(m *testing.M) {
	tmp, err := os.MkdirTemp(os.TempDir(), "bk")
	if err != nil {
		panic(err)
	}

	dbPath := filepath.Join(tmp, ".bk.db")
	db, err = Load(dbPath)
	if err != nil {
		panic(err)
	}

	status := m.Run()

	if err := os.RemoveAll(tmp); err != nil {
		panic(err)
	}

	os.Exit(status)
}

func TestBackup_BKPath(t *testing.T) {
	bk := Backup{Path: "test"}
	if bk.BKPath() != "test.bk" {
		t.Log("BKPath should append .bk to file path")
		t.FailNow()
	}

	bk = Backup{Path: "testing/test"}
	if bk.BKPath() != "testing/test.bk" {
		t.Log("BKPath should append .bk to file path")
		t.FailNow()
	}
}

func TestDatabase(t *testing.T) {
	// Add
	bk := Backup{
		Path:       "test",
		Expiration: time.Now(),
	}
	add(t, &bk)

	// List
	list(t, 1)

	// Remove
	remove(t, bk.Seq)

	// List
	list(t, 0)
}

func add(t *testing.T, bk *Backup) {
	seq, err := db.AddBackup(*bk)
	if err != nil {
		t.Log(err)
		t.FailNow()
	}
	bk.Seq = []byte(strconv.FormatUint(seq, 10))
}

func list(t *testing.T, numBackups int) {
	backups, err := db.ListBackups()
	if err != nil {
		t.Log(err)
		t.FailNow()
	}

	if len(backups) != numBackups {
		t.Logf("Expected %d backups but got %d\n", numBackups, len(backups))
		t.FailNow()
	}
}

func remove(t *testing.T, seq []byte) {
	if err := db.RemoveBackups(seq); err != nil {
		t.Log(err)
		t.FailNow()
	}
}
